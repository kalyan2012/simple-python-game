import pygame

display_height = 800
display_width = 1000
player_1 = pygame.image.load("up.png")
player_2 = pygame.image.load("down.png")
ship = pygame.image.load("ship.png")
tank = pygame.image.load("tank.png")
x_init_player_1 = 520
y_init_player_1 = 750
x_init_player_2 = 520
y_init_player_2 = 0
brown = (181, 101, 30)
blue = (0, 0, 255)
black = (0, 0, 0)


def vel(x, le, i, pl):
    if pl == 2:
        return (x + le / 2 + i) % 1000
    else:
        return (x + le / 2 + 10 - i) % 1000


def text(txt, size):
    font_text = pygame.font.Font('freesansbold.ttf', int(size))
    surface = font_text.render(txt, True, black)
    return surface, surface.get_rect()

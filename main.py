import random
import time
from config import *

pygame.init()

gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()


def stand_position(position):
    pygame.draw.rect(gameDisplay, brown, (0, position, display_width, 40), 0)


level_player1 = 1
level_player2 = 1
score_1 = 0
score_2 = 0
time_start_1 = 0
time_start_2 = 0


def game_loop_1():
    global level_player1, score_1
    global time_start_1
    time_start_1 = time.time()
    crash = False
    ship_x = []
    score = 0
    rand_x = x_init_player_1
    rand_y = y_init_player_1
    ship_y = [50, 100, 150, 250, 350, 400, 450, 550, 600, 700, 200, 300, 500, 650]
    place = ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'l', 'l', 'l', 'l']
    for i in range(0, 14):
        ship_x.append(random.randint(10, 950))
    while not crash:
        gameDisplay.fill(blue)
        for i in range(0, 10):
            ship_x[i] = vel(ship_x[i], level_player2, i, 1)
        if rand_y == 0:
            level_player1 = level_player1 + 1
        if rand_y == 0:
            rand_y = 750
            rand_x = 520
            textsurf, textrect = text("LEVELUP!", 100)
            textrect.center = ((display_width / 2), (display_height / 2))
            gameDisplay.blit(textsurf, textrect)
            pygame.display.update()
            time.sleep(0.25)
            for i in range(0, 14):
                ship_x[i] = random.randint(10, 950)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                crash = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP and rand_y > 0:
                    for i in range(0, 14):
                        if rand_y == ship_y[i]:
                            if place[i] == 'l':
                                score = score + 5
                            if place[i] == 'w':
                                score = score + 10
                    rand_y = rand_y - 50
                if event.key == pygame.K_DOWN and rand_y < display_height - 64:
                    for i in range(0, 14):
                        if rand_y + 50 == ship_y[i]:
                            if place[i] == 'l':
                                score = score - 5
                            if place[i] == 'w':
                                score = score - 10
                    rand_y = rand_y + 50
                if event.key == pygame.K_LEFT and rand_x > 0:
                    rand_x = rand_x - 52
                if event.key == pygame.K_RIGHT and rand_x < display_width - 64:
                    rand_x = rand_x + 52
        for i in range(0, 14):
            if ship_y[i] == rand_y:
                if rand_x <= ship_x[i] + 35:
                    if rand_x + 10 >= ship_x[i]:
                        time_start_1 = time.time() - time_start_1
                        textsurf, textrect = text("oops you got ran over!", 50)
                        textrect.center = (display_width / 2, display_height / 2)
                        gameDisplay.blit(textsurf, textrect)
                        score_1 = score
                        pygame.display.update()
                        time.sleep(1)
                        del ship_x[:]
                        del place[:]
                        game_loop_2()
                if rand_x + 50 <= ship_x[i] + 35:
                    if rand_x + 50 >= ship_x[i]:
                        time_start_1 = time.time() - time_start_1
                        textsurf, textrect = text("oops you got ran over!", 50)
                        textrect.center = (display_width / 2, display_height / 2)
                        gameDisplay.blit(textsurf, textrect)
                        score_1 = score
                        pygame.display.update()
                        time.sleep(1)
                        del ship_x[:]
                        del place[:]
                        game_loop_2()
        stand_position(display_height - 40)
        stand_position(display_height - 140)
        stand_position(display_height - 290)
        stand_position(display_height - 490)
        stand_position(display_height - 590)
        stand_position(10)
        for i in range(0, 10):
            gameDisplay.blit(ship, (ship_x[i], ship_y[i]))
        for i in range(10, 14):
            gameDisplay.blit(tank, (ship_x[i], ship_y[i]))
        textsurf, textrect = text("START", 25)
        textrect.center = (display_width / 2 + 130, 780)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text("END", 25)
        textrect.center = (display_width / 2 + 110, 30)
        gameDisplay.blit(textsurf, textrect)
        gameDisplay.blit(player_1, (rand_x, rand_y))
        textsurf, textrect = text("LEVEL:", 25)
        textrect.center = (display_width - 100, 30)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text(str(level_player1), 25)
        textrect.center = (display_width - 40, 30)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text("SCORE:  ", 25)
        textrect.center = (display_width - 100, 60)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text(str(score), 25)
        textrect.center = (display_width - 40, 60)
        gameDisplay.blit(textsurf, textrect)
        pygame.display.update()
        clock.tick(30)
    pygame.quit()
    quit()


def game_loop_2():
    global level_player2, score_2
    global time_start_2
    time_start_2 = time.time()
    crash = False
    ship_x = []
    score = 0
    rand_x = x_init_player_2
    rand_y = y_init_player_2
    ship_y = [50, 100, 150, 250, 350, 400, 450, 550, 600, 700, 200, 300, 500, 650]
    place = ['w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'l', 'l', 'l', 'l']
    for i in range(0, 14):
        ship_x.append(random.randint(10, 950))
    while not crash:
        gameDisplay.fill(blue)
        for i in range(0, 10):
            ship_x[i] = vel(ship_x[i], level_player2, i, 2)
        if rand_y == 750:
            level_player2 = level_player2 + 1
            rand_y = 0
            rand_x = 520
            textsurf, textrect = text("LEVELUP!", 100)
            textrect.center = ((display_width / 2), (display_height / 2))
            gameDisplay.blit(textsurf, textrect)
            pygame.display.update()
            time.sleep(0.25)
            for i in range(0, 14):
                ship_x[i] = random.randint(10, 950)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                crash = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP and rand_y > 0:
                    for i in range(0, 14):
                        if rand_y - 50 == ship_y[i]:
                            if place[i] == 'l':
                                score = score - 5
                            if place[i] == 'w':
                                score = score - 10
                    rand_y = rand_y - 50
                if event.key == pygame.K_DOWN and rand_y < display_height - 64:
                    for i in range(0, 14):
                        if rand_y == ship_y[i]:
                            if place[i] == 'l':
                                score = score + 5
                            if place[i] == 'w':
                                score = score + 10
                    rand_y = rand_y + 50
                if event.key == pygame.K_LEFT and rand_x > 0:
                    rand_x = rand_x - 52
                if event.key == pygame.K_RIGHT and rand_x < display_width - 64:
                    rand_x = rand_x + 52
        for i in range(0, 14):
            if ship_y[i] == rand_y:
                if rand_x <= ship_x[i] + 35 and rand_x + 10 >= ship_x[i]:
                    time_start_2 = time.time() - time_start_2
                    textsurf, textrect = text("oops you ran over!", 50)
                    textrect.center = (display_width / 2, display_height / 2)
                    gameDisplay.blit(textsurf, textrect)
                    score_2 = score
                    pygame.display.update()
                    time.sleep(0.25)
                    del ship_x[:]
                    del place[:]
                    gameDisplay.fill(blue)
                    textsurf, textrect = text("SCORE for PLAYER 1:", 30)
                    textrect.center = (display_width / 2 - 100, display_height / 2 - 100)
                    gameDisplay.blit(textsurf, textrect)
                    textsurf, textrect = text(str(score_1), 30)
                    textrect.center = (display_width / 2 + 100, display_height / 2 - 100)
                    gameDisplay.blit(textsurf, textrect)
                    textsurf, textrect = text("SCORE for PLAYER 2:", 30)
                    textrect.center = (display_width / 2 - 100, display_height / 2)
                    gameDisplay.blit(textsurf, textrect)
                    textsurf, textrect = text(str(score_2), 30)
                    textrect.center = (display_width / 2 + 100, display_height / 2)
                    gameDisplay.blit(textsurf, textrect)
                    pygame.display.update()
                    time.sleep(1)
                    gameDisplay.fill(blue)
                    if score_1 + score_2:
                        if score_1 > score_2:
                            textsurf, textrect = text("winner is 1!", 100)
                            textrect.center = (display_width / 2, display_height / 2)
                            gameDisplay.blit(textsurf, textrect)
                        elif score_1 < score_2:
                            textsurf, textrect = text("winner is 2!", 100)
                            textrect.center = (display_width / 2, display_height / 2)
                            gameDisplay.blit(textsurf, textrect)
                        else:
                            if time_start_1 < time_start_2:
                                textsurf, textrect = text("but 1 took less time", 100)
                                textrect.center = (display_width / 2, display_height / 2)
                                gameDisplay.blit(textsurf, textrect)
                                pygame.display.update()
                                time.sleep(1)
                                gameDisplay.fill(blue)
                                textsurf, textrect = text("winner is 1!", 100)
                                textrect.center = (display_width / 2, display_height / 2)
                                gameDisplay.blit(textsurf, textrect)
                            if time_start_1 > time_start_2:
                                textsurf, textrect = text("but 2 took less time", 100)
                                textrect.center = (display_width / 2, display_height / 2)
                                gameDisplay.blit(textsurf, textrect)
                                pygame.display.update()
                                time.sleep(1)
                                gameDisplay.fill(blue)
                                textsurf, textrect = text("winner is 2!", 100)
                                textrect.center = (display_width / 2, display_height / 2)
                                gameDisplay.blit(textsurf, textrect)
                            elif time_start_2 == time_start_1:
                                textsurf, textrect = text("tie!", 100)
                                textrect.center = (display_width / 2, display_height / 2)
                                gameDisplay.blit(textsurf, textrect)
                        pygame.display.update()
                        time.sleep(1)
                        gameDisplay.fill(blue)
                    else:
                        textsurf, textrect = text("no winner!", 50)
                        textrect.center = (display_width / 2, display_height / 2)
                        gameDisplay.blit(textsurf, textrect)
                        pygame.display.update()
                        time.sleep(1)
                    gameDisplay.fill(blue)
                    textsurf, textrect = text("q: quit, r:restart", 50)
                    textrect.center = (display_width / 2, display_height / 2)
                    gameDisplay.blit(textsurf, textrect)
                    pygame.display.update()
                    time.sleep(1)
                    while True:
                        for event in pygame.event.get():
                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_r:
                                    game_loop_1()
                                if event.key == pygame.K_q:
                                    quit()
                if rand_x + 50 <= ship_x[i] + 35:
                    if rand_x + 50 >= ship_x[i]:
                        time_start_2 = time.time() - time_start_2
                        textsurf, textrect = text("oops you got ran over!", 50)
                        textrect.center = (display_width / 2, display_height / 2)
                        gameDisplay.blit(textsurf, textrect)
                        score_2 = score
                        pygame.display.update()
                        time.sleep(0.25)
                        del ship_x[:]
                        del place[:]
                        gameDisplay.fill(blue)
                        textsurf, textrect = text("SCORE for PLAYER 1:", 30)
                        textrect.center = (display_width / 2 - 100, display_height / 2 - 100)
                        gameDisplay.blit(textsurf, textrect)
                        textsurf, textrect = text(str(score_1), 30)
                        textrect.center = (display_width / 2 + 100, display_height / 2 - 100)
                        gameDisplay.blit(textsurf, textrect)
                        textsurf, textrect = text("SCORE for PLAYER 2:", 30)
                        textrect.center = (display_width / 2 - 100, display_height / 2)
                        gameDisplay.blit(textsurf, textrect)
                        textsurf, textrect = text(str(score_2), 30)
                        textrect.center = (display_width / 2 + 100, display_height / 2)
                        gameDisplay.blit(textsurf, textrect)
                        pygame.display.update()
                        time.sleep(1)
                        gameDisplay.fill(blue)
                        if score_1 + score_2:
                            if score_1 > score_2:
                                textsurf, textrect = text("winner is 1!", 100)
                                textrect.center = (display_width / 2, display_height / 2)
                                gameDisplay.blit(textsurf, textrect)
                            elif score_1 < score_2:
                                textsurf, textrect = text("winner is 2!", 100)
                                textrect.center = (display_width / 2, display_height / 2)
                                gameDisplay.blit(textsurf, textrect)
                            else:
                                if time_start_1 < time_start_2:
                                    textsurf, textrect = text("but 1 took less time", 100)
                                    textrect.center = (display_width / 2, display_height / 2)
                                    gameDisplay.blit(textsurf, textrect)
                                    pygame.display.update()
                                    time.sleep(1)
                                    gameDisplay.fill(blue)
                                    textsurf, textrect = text("winner is 1!", 100)
                                    textrect.center = (display_width / 2, display_height / 2)
                                    gameDisplay.blit(textsurf, textrect)
                                if time_start_1 > time_start_2:
                                    textsurf, textrect = text("but 2 took less time", 100)
                                    textrect.center = (display_width / 2, display_height / 2)
                                    gameDisplay.blit(textsurf, textrect)
                                    pygame.display.update()
                                    time.sleep(1)
                                    gameDisplay.fill(blue)
                                    textsurf, textrect = text("winner is 2!", 100)
                                    textrect.center = (display_width / 2, display_height / 2)
                                    gameDisplay.blit(textsurf, textrect)
                                elif time_start_1 == time_start_2:
                                    textsurf, textrect = text("tie!", 100)
                                    textrect.center = (display_width / 2, display_height / 2)
                                    gameDisplay.blit(textsurf, textrect)
                            pygame.display.update()
                            time.sleep(1)
                            gameDisplay.fill(blue)
                        else:
                            textsurf, textrect = text("no winner!", 50)
                            textrect.center = (display_width / 2, display_height / 2)
                            gameDisplay.blit(textsurf, textrect)
                            pygame.display.update()
                            time.sleep(1)
                        gameDisplay.fill(blue)
                        textsurf, textrect = text("q: quit, r:restart", 50)
                        textrect.center = (display_width / 2, display_height / 2)
                        gameDisplay.blit(textsurf, textrect)
                        pygame.display.update()
                        time.sleep(1)
                        while True:
                            for event in pygame.event.get():
                                if event.type == pygame.KEYDOWN:
                                    if event.key == pygame.K_r:
                                        game_loop_1()
                                    if event.key == pygame.K_q:
                                        quit()
        stand_position(display_height - 40)
        stand_position(display_height - 140)
        stand_position(display_height - 290)
        stand_position(display_height - 490)
        stand_position(display_height - 590)
        stand_position(10)
        for i in range(0, 10):
            gameDisplay.blit(ship, (ship_x[i], ship_y[i]))
        for i in range(10, 14):
            gameDisplay.blit(tank, (ship_x[i], ship_y[i]))
        textsurf, textrect = text("END", 25)
        textrect.center = (display_width / 2 + 130, 780)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text("START", 25)
        textrect.center = (display_width / 2 + 120, 30)
        gameDisplay.blit(textsurf, textrect)
        gameDisplay.blit(player_2, (rand_x, rand_y))
        textsurf, textrect = text("LEVEL:", 25)
        textrect.center = (display_width - 100, 30)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text(str(level_player2), 25)
        textrect.center = (display_width - 40, 30)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text("SCORE:  ", 25)
        textrect.center = (display_width - 100, 60)
        gameDisplay.blit(textsurf, textrect)
        textsurf, textrect = text(str(score), 25)
        textrect.center = (display_width - 40, 60)
        gameDisplay.blit(textsurf, textrect)
        pygame.display.update()
        clock.tick(30)
    pygame.quit()
    quit()


game_loop_1()
